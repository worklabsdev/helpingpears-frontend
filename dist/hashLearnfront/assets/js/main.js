 var jQuery = $;
 
jQuery(document).ready(function(){
      jQuery('.hero-element-outer').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      autoplay: true,
       fade: true,
      autoplaySpeed: 2000,
       cssEase: 'linear'
      });
       jQuery('#feature-slider').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 2000,
      });
      jQuery('.testimonials-outer').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
         autoplay: true,
        autoplaySpeed: 4000
        });
  
    });

    jQuery(document).ready(function(){
      jQuery(window).scrollTop(0);
  });