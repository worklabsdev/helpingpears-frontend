import { Injectable } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
import { GlobalService } from "./global.service";
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: "root"
})
export class StudentService {
  constructor(public http: Http, public globalservice: GlobalService) {}

  authenticate(authdata) {
    return this.http
      .post(APIEndpoint + "stu/login", authdata)
      .pipe(map(res => res.json()));
  }
  register(data) {
    return this.http
      .post(APIEndpoint + "stu/register", data)
      .pipe(map(res => res.json()));
  }

  getstudentProfileData(studentid) {
    return this.http
      .get(APIEndpoint + "stu/getstudentProfileData/" + studentid)
      .pipe(map(res => res.json()));
  }
  savequery(data) {
    return this.http
      .post(APIEndpoint + "stu/queryInsert", data)
      .pipe(map(res => res.json()));
  }
  getqueries(studentid) {
    return this.http
      .get(APIEndpoint + "stu/studentQueries/" + studentid)
      .pipe(map(res => res.json()));
  }
  addcard(data) {
    return this.http
      .post(APIEndpoint + "stu/addcard", data)
      .pipe(map(res => res.json()));
  }
  chargepayment(data) {
    return this.http
      .post(APIEndpoint + "stu/chargecard", data)
      .pipe(map(res => res.json()));
  }

  paymentStatusset(data) {
    return this.http
      .post(APIEndpoint + "stu/paymentStatusset", data)
      .pipe(map(res => res.json()));
  }

  getsinglequery(slug) {
    return this.http
      .get(APIEndpoint + "stu/getsinglequery/" + slug)
      .pipe(map(res => res.json()));
  }
  editprofile(id, data) {
    return this.http
      .put(APIEndpoint + "stu/editprofile/" + id, data)
      .pipe(map(res => res.json()));
  }
  getStudentsQueries() {
    return this.http
      .get(APIEndpoint + "stu/getstudentsqueries")
      .pipe(map(res => res.json()));
  }
  uploadProfileImage(id, data) {
    return this.http
      .put(APIEndpoint + "stu/uploadprofileimage/" + id, data)
      .pipe(map(res => res.json()));
  }
  askQueryToAdmin(data) {
    return this.http
      .post(APIEndpoint + "stu/askquerytoadmin", data)
      .pipe(map(res => res.json()));
  }
  getStudentAdminQueries(id) {
    return this.http
      .get(APIEndpoint + "stu/getstudentadminqueries/" + id)
      .pipe(map(res => res.json()));
  }
  getSingleAdminQuery(id) {
    return this.http
      .get(APIEndpoint + "stu/getsingleadminquery/" + id)
      .pipe(map(res => res.json()));
  }
  removeStudentDp(id) {
    return this.http
      .get(APIEndpoint + "stu/removestudentdp/" + id)
      .pipe(map(res => res.json()));
  }
  studentReply(id, data) {
    return this.http
      .put(APIEndpoint + "stu/studentreply/" + id, data)
      .pipe(map(res => res.json()));
  }
  rateStudent(slug, data) {
    return this.http
      .put(APIEndpoint + "stu/ratestudent/" + slug, data)
      .pipe(map(res => res.json()));
  }
  checkStudentEmail(email) {
    return this.http
      .get(APIEndpoint + "stu/checkStudentEmail/" + email)
      .pipe(map(res => res.json()));
  }
  messageToTeacher(slug, data) {
    return this.http
      .put(APIEndpoint + "stu/messagetoteacher/" + slug, data)
      .pipe(map(res => res.json()));
  }
  rateTeacher(slug, data) {
    return this.http
      .put(APIEndpoint + "stu/ratestudent/" + slug, data)
      .pipe(map(res => res.json()));
  }
}
