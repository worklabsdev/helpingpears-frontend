import { Injectable } from "@angular/core";
import { HttpModule, Http } from "@angular/http";
import { GlobalService } from "./global.service";
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Injectable({
  providedIn: "root"
})
export class TeacherService {
  constructor(public http: Http, public globalservice: GlobalService) {}

  authenticate(authdata) {
    return this.http
      .post(APIEndpoint + "t/login", authdata)
      .pipe(map(res => res.json()));
  }
  register(data) {
    return this.http
      .post(APIEndpoint + "t/register", data)
      .pipe(map(res => res.json()));
  }
  getteacherProfileData(id) {
    return this.http
      .get(APIEndpoint + "t/getteacherProfileData/" + id)
      .pipe(map(res => res.json()));
  }
  editprofile(id, data) {
    return this.http
      .put(APIEndpoint + "t/editprofile/" + id, data)
      .pipe(map(res => res.json()));
  }
  uploaddoc(id, data) {
    return this.http
      .put(APIEndpoint + "t/uploaddoc/" + id, data)
      .pipe(map(res => res.json()));
  }
  uploadProfileImage(id, data) {
    return this.http
      .put(APIEndpoint + "t/uploadprofileimage/" + id, data)
      .pipe(map(res => res.json()));
  }
  removeprofileImage(id) {
    return this.http
      .get(APIEndpoint + "t/removeprofileimage/" + id)
      .pipe(map(res => res.json()));
  }
  askQueryToAdmin(data) {
    return this.http
      .post(APIEndpoint + "t/askquerytoadmin", data)
      .pipe(map(res => res.json()));
  }
  getTeacherAdminQueries(id) {
    return this.http
      .get(APIEndpoint + "t/getteacheradminqueries/" + id)
      .pipe(map(res => res.json()));
  }
  getSingleAdminQuery(id) {
    return this.http
      .get(APIEndpoint + "t/getsingleadminquery/" + id)
      .pipe(map(res => res.json()));
  }
  teacherReply(id, data) {
    return this.http
      .put(APIEndpoint + "t/teacherreply/" + id, data)
      .pipe(map(res => res.json()));
  }
  applyForJob(slug, data) {
    return this.http
      .put(APIEndpoint + "t/applyforjob/" + slug, data)
      .pipe(map(res => res.json()));
  }
  getMyJobs(id) {
    return this.http
      .get(APIEndpoint + "t/getmyjobs/" + id)
      .pipe(map(res => res.json()));
  }
  messageToStudent(slug, data) {
    return this.http
      .put(APIEndpoint + "t/messagetostudent/" + slug, data)
      .pipe(map(res => res.json()));
  }
  sendTeacherProfileForReview(id) {
    return this.http
      .get(APIEndpoint + "t/sendteacherprofileforreview/" + id)
      .pipe(map(res => res.json()));
  }
  startclass(slug) {
    return this.http
      .request(APIEndpoint + "t/createsession/" + slug)
      .pipe(map(res => res.json()));
  }
  CreateUserStripe(tokendata) {
    console.log("you have the token data");
    return this.http
      .get(APIEndpoint + "t/createuserstripe/" + tokendata)
      .pipe(map(res => res.json()));
  }
  // startcall(slug){
  //   console.log("this is start call slug",slug);
  //   return this.http.get(APIEndpoint + 't/createsession/'+slug)
  //   .pipe(map(res => res.json()));
  // }
  // askQueryToAdmin(data) {
  //   return this.http.post(APIEndpoint + 't/askquerytoadmin',data)
  //   .pipe(map(res => res.json()));
  // }
}
