import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../providers/student.service';
import {DomSanitizer}  from '@angular/platform-browser';
@Component({
  selector: 'app-pqueries',
  templateUrl: './pqueries.component.html',
  styleUrls: ['./pqueries.component.css']
})
export class PqueriesComponent implements OnInit {
queryData:any;
  constructor(public studentservice :StudentService,public sanetizer:DomSanitizer) { }

  ngOnInit() {
    this.getStudentsQueries();
  }

  getStudentsQueries() {
    this.studentservice.getStudentsQueries().subscribe((data:any)=>{
      console.log("thisi s the data",data);
      this.queryData = data.data;
      for(var t=0;t<this.queryData.length;t++){
        this.queryData[t].question = this.sanetizer.bypassSecurityTrustUrl(this.queryData[t].question);
      }
    },(error:any)=>{
      console.log("this is error",error);
    })
  }


}
