import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PqueriesComponent } from './pqueries.component';

describe('PqueriesComponent', () => {
  let component: PqueriesComponent;
  let fixture: ComponentFixture<PqueriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PqueriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PqueriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
