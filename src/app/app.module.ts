import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ModalModule } from "ngx-bootstrap";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { AppRouting } from "./app.router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { GrowlModule } from "primeng/growl";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EditorModule } from "primeng/editor";
import { RadioButtonModule } from "primeng/radiobutton";
import { TagInputModule } from "ngx-chips";
import { TimeAgoPipe } from "time-ago-pipe";

import { AngularAgoraRtcModule, AgoraConfig } from "angular-agora-rtc";

import { WebcamModule } from "ngx-webcam";

import { BarRatingModule } from "ngx-bar-rating";
import { RatingModule } from "primeng/rating";
import { FieldsetModule } from "primeng/fieldset";
import { SlickModule } from "ngx-slick";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { ToastModule } from "primeng/toast";
import { MessageService } from "primeng/api";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { ProfileComponent } from "./student/profile/profile.component";
import { HomeComponent } from "./home/home.component";
import { LoginheaderComponent } from "./student/navigation/loginheader/loginheader.component";
import { MainheaderComponent } from "./mainheader/mainheader.component";
import { FooterComponent } from "./footer/footer.component";
import { EditprofileComponent } from "./student/editprofile/editprofile.component";
import { StucontactComponent } from "./student/stucontact/stucontact.component";
import { SsidebarComponent } from "./student/ssidebar/ssidebar.component";
import { AskqueryComponent } from "./student/askquery/askquery.component";
import { SinglequeryComponent } from "./student/singlequery/singlequery.component";
import { LoginenableComponent } from "./student/loginenable/loginenable.component";
import { PqueriesComponent } from "./public/pqueries/pqueries.component";
import { TloginComponent } from "./teacher/tlogin/tlogin.component";
import { TprofileComponent } from "./teacher/tprofile/tprofile.component";
import { TloginheaderComponent } from "./teacher/navigation/tloginheader/tloginheader.component";
import { TeditprofileComponent } from "./teacher/teditprofile/teditprofile.component";
import { TsidebarComponent } from "./teacher/tsidebar/tsidebar.component";
import { TregisterComponent } from "./teacher/tregister/tregister.component";
import { RegisterComponent } from "./student/register/register.component";
import { SAdminQUeryComponent } from "./student/AdminQuery/s-admin-query/s-admin-query.component";
import { TContactAdminComponent } from "./teacher/t-contact-admin/t-contact-admin.component";
import { TsAdminQueryComponent } from "./teacher/ts-admin-query/ts-admin-query.component";
import { QueryjobsComponent } from "./teacher/queryjobs/queryjobs.component";
import { SingleQueryJobComponent } from "./teacher/single-query-job/single-query-job.component";
import { TMyJobsComponent } from "./teacher/t-my-jobs/t-my-jobs.component";
import { ModeratorComponent } from "./teacher/moderator/moderator.component";
import { PriceComponent } from "./pricing/pricing.components";
import { OurVisionComponent } from "./our-vision/our-vision.component";
import { PreRegisterComponent } from "./register/register.component";
import { CameraComponent } from "./camera/camera.component";

const agoraConfig: AgoraConfig = {
  AppID: "482f3b0826bb40758dbbe87e5058d7ef"
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    HomeComponent,
    LoginheaderComponent,
    MainheaderComponent,
    FooterComponent,
    EditprofileComponent,
    StucontactComponent,
    SsidebarComponent,
    AskqueryComponent,
    SinglequeryComponent,
    LoginenableComponent,
    PqueriesComponent,
    TloginComponent,
    TprofileComponent,
    TloginheaderComponent,
    TeditprofileComponent,
    TsidebarComponent,
    TregisterComponent,
    RegisterComponent,
    SAdminQUeryComponent,
    TContactAdminComponent,
    TsAdminQueryComponent,
    QueryjobsComponent,
    SingleQueryJobComponent,
    TimeAgoPipe,
    TMyJobsComponent,
    ModeratorComponent,
    PriceComponent,
    OurVisionComponent,
    PreRegisterComponent,
    CameraComponent
  ],
  imports: [
    BrowserModule,
    AppRouting,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ToastModule,
    BrowserAnimationsModule,
    GrowlModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TagInputModule,
    EditorModule,
    NgMultiSelectDropDownModule.forRoot(),
    RadioButtonModule,
    BarRatingModule,
    RatingModule,
    SlickModule.forRoot(),
    FieldsetModule,
    AngularAgoraRtcModule.forRoot(agoraConfig),
    WebcamModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent]
})
export class AppModule {}
