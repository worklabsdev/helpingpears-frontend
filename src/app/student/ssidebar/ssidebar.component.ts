import { Component, OnInit } from "@angular/core";
import { StudentService } from "../../providers/student.service";
import { GlobalService } from "../../providers/global.service";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-ssidebar",
  templateUrl: "./ssidebar.component.html",
  styleUrls: ["./ssidebar.component.css"]
})
export class SsidebarComponent implements OnInit {
  dpUrl: String;
  studentData: any;
  dp_64textString: String;
  imageLoader: Boolean = false;
  constructor(
    public studentService: StudentService,
    public globalService: GlobalService,
    private messageService: MessageService
  ) {
    this.dpUrl = APIEndpoint + "public/student/dp/";
  }
  ngOnInit() {
    this.getstudentdata();
  }

  getstudentdata() {
    var userid = localStorage.user_id;
    this.studentService.getstudentProfileData(userid).subscribe(
      (data: any) => {
        console.log("this is the data", data);
        this.studentData = data.data[0];
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.dp_64textString = btoa(binaryString);
    this.dp_64textString = "data:image/png;base64," + this.dp_64textString;
    // console.log(btoa(binaryString));
  }

  uploadProfileImage() {
    this.imageLoader = true;
    var postData = {
      profileImage: this.dp_64textString
    };
    var id = localStorage.user_id;
    if (this.dp_64textString) {
      this.studentService.uploadProfileImage(id, postData).subscribe(
        (data: any) => {
          console.log("this is the data", data);
          this.imageLoader = false;
          this.dp_64textString = "";
          this.getstudentdata();
          this.messageService.add({
            severity: "success",
            summary: "Profile Image Updated"
          });
        },
        (error: any) => {
          console.log("this is the error", error);
        }
      );
    }
  }
  removeStudentDp() {
    var id = localStorage.user_id;
    if (id) {
      this.studentService.removeStudentDp(id).subscribe(
        (data: any) => {
          this.messageService.add({
            severity: "success",
            summary: "Profile Image Removed"
          });
          this.dp_64textString = "";
          this.getstudentdata();
          console.log("this is daata", data);
        },
        (error: any) => {
          console.log("error", error);
        }
      );
    }
  }
}
