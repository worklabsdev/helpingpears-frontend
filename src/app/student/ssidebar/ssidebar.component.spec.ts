import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SsidebarComponent } from './ssidebar.component';

describe('SsidebarComponent', () => {
  let component: SsidebarComponent;
  let fixture: ComponentFixture<SsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
