import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../providers/global.service';
import { StudentService } from '../../../providers/student.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { ActivatedRoute,RouterModule,Router} from  '@angular/router';
@Component({
  selector: 'app-s-admin-query',
  templateUrl: './s-admin-query.component.html',
  styleUrls: ['./s-admin-query.component.css']
})
export class SAdminQUeryComponent implements OnInit {
  querySentLoading:Boolean=false;
  qryAdminData:any;
  noDataFlag:Boolean = false;
  qryForm : FormGroup;
  msgs:Message[]=[];
  constructor(public globalService:GlobalService,public studentService:StudentService
    ,private messageService: MessageService,public activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.qryForm = new FormGroup({
      replytext: new FormControl('',Validators.required)
    })

    this.getSingleAdminQuery();
  }

  getSingleAdminQuery() {
    var queryid = this.activatedRoute.snapshot.paramMap.get('id');
    this.studentService.getSingleAdminQuery(queryid).subscribe((data:any)=>{
      console.log("this is the datasdfasd",data);
      if(data.status==200){
        this.qryAdminData = data.data;
      }else{
        this.noDataFlag = true;
      }

    },(error:any)=>{
      console.log("this is the error",error);
    })
  }
  studentReply() {
    if(this.qryForm.valid){
      var postData = {
        userType : 'student',
        replyText: this.qryForm.value.replytext,
        repliedAt : new Date()
      }
      var queryid = this.activatedRoute.snapshot.paramMap.get('id');
      this.studentService.studentReply(queryid,postData).subscribe((data:any)=>{
        this.getSingleAdminQuery();
        console.log("this is data",data);
      },(error:any)=>{
        console.log("this is error",error);
      })
    }
  }
}
