import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SAdminQUeryComponent } from './s-admin-query.component';

describe('SAdminQUeryComponent', () => {
  let component: SAdminQUeryComponent;
  let fixture: ComponentFixture<SAdminQUeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SAdminQUeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SAdminQUeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
