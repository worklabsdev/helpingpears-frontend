import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglequeryComponent } from './singlequery.component';

describe('SinglequeryComponent', () => {
  let component: SinglequeryComponent;
  let fixture: ComponentFixture<SinglequeryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglequeryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglequeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
