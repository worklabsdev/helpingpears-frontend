import { Component, OnInit } from "@angular/core";
import { StudentService } from "../../providers/student.service";
import { GlobalService } from "../../providers/global.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";
import { AngularAgoraRtcService, Stream } from "angular-agora-rtc";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

declare function join(email): any;
declare function leave(): any;
// declare function getDevices():any;
declare function detect(): any;
// declare function timeConsumed():any;

declare var formComponent1: any;

@Component({
  selector: "app-singlequery",
  templateUrl: "./singlequery.component.html",
  styleUrls: ["./singlequery.component.css"]
})
export class SinglequeryComponent implements OnInit {
  queryData: any;
  slug: any;
  qryForm: FormGroup;
  ratingForm: FormGroup;
  msgs: Message[] = [];
  tDpUrl: any;
  queryImageUrl: String;
  title = "AgoraDemo";
  localStream: Stream;
  remoteCalls: any = [];

  querySentLoading: any;
  constructor(
    public studentService: StudentService,
    public sanetizer: DomSanitizer,
    public activatedRoute: ActivatedRoute,
    public globalService: GlobalService,
    // private agoraService: AngularAgoraRtcService,
    private agoraService: AngularAgoraRtcService
  ) {
    this.agoraService.createClient();
    this.slug = this.activatedRoute.snapshot.params.slug;
    this.queryImageUrl = APIEndpoint + "public/queryimages/";
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    formComponent1 = this;

    this.ratingForm = new FormGroup({
      rating: new FormControl("", [Validators.required]),
      comment: new FormControl("", [Validators.required])
    });
    this.qryForm = new FormGroup({
      replyChat: new FormControl("", [Validators.required])
    });
    this.tDpUrl = APIEndpoint + "public/teacher/dp/";
    this.getSingleStudentsQueries();
  }
  ngOnDestroy(): any {
    formComponent1 = null;
  }
  doAngularThing() {
    console.log("Typescript called from JS:");
  }

  join() {
    var email = localStorage.getItem("user_email");
    var body = {
      status: false,
      id: localStorage.getItem("user_id")
    };
    this.studentService.paymentStatusset(body).subscribe(
      (data: any) => {
        console.log("thisi s the payment", data);
        if (data.message == "success") {
          join(email);
        }
      },
      (error: any) => {
        console.log("this is error", error);
        // timeConsumed();
      }
    );
    console.log("email", email);
  }

  leave() {
    leave();
    setTimeout(() => {
      //  alert("Hello");
      // this.payment(localStorage.getItem('totalTimeCall'));
    }, 2000);
  }

  payment(time) {
    // localStorage.removeItem('totalTimeCall');
    var body = {
      email: localStorage.getItem("user_email"),
      amount: time * 80,
      status: true,
      id: localStorage.getItem("user_id")
    };
    this.studentService.paymentStatusset(body).subscribe(
      (data: any) => {
        console.log("thisi s the payment", data);
        if (data.message == "false") {
          this.studentService.chargepayment(body).subscribe(
            (data: any) => {
              console.log("thisi s the paymentttttttttt", data);
            },
            (error: any) => {
              console.log("this is error", error);
              // timeConsumed();
            }
          );
        }
      },
      (error: any) => {
        console.log("this is error", error);
        // timeConsumed();
      }
    );
  }

  getSingleStudentsQueries() {
    this.studentService.getsinglequery(this.slug).subscribe(
      (data: any) => {
        console.log("thisi s the data", data);
        this.queryData = data.data;
        console.log("this is the query Data", this.queryData);
        this.queryData.question = this.sanetizer.bypassSecurityTrustUrl(
          this.queryData.question
        );
        detect();
      },
      (error: any) => {
        console.log("this is error", error);
        detect();
      }
    );
  }
  // applyForJob(){
  //   var postData = {
  //     selectedteacher:localStorage.user_id
  //   }
  //   this.studentService.applyForJob(this.slug,postData).subscribe((data:any)=>{
  //     console.log("this is data",data);
  //   },(error:any)=>{
  //     console.log("this is error",error);
  //   })
  // }
  rateTeacher() {
    if (this.ratingForm.valid) {
      var postData = {
        rating: this.ratingForm.value.rating,
        comment: this.ratingForm.value.comment
      };
      this.studentService.rateStudent(this.slug, postData).subscribe(
        (data: any) => {
          console.log("this is ithe data", data);
        },
        (error: any) => {
          console.log("this is error", error);
        }
      );
    }
  }

  messageToTeacher() {
    if (this.qryForm.valid) {
      var postData = {
        userType: "student",
        replyText: this.qryForm.value.replyChat,
        repliedAt: new Date()
      };
      var slug = this.activatedRoute.snapshot.paramMap.get("slug");
      this.studentService.messageToTeacher(slug, postData).subscribe(
        (data: any) => {
          this.qryForm.reset();
          this.getSingleStudentsQueries();
        },
        (error: any) => {
          console.log("this is error", error);
        }
      );
    }
  }

  startcall() {
    this.agoraService.client.join(null, "1000", null, uid => {
      this.localStream = this.agoraService.createStream(
        uid,
        true,
        null,
        null,
        true,
        false
      );
      this.localStream.setVideoProfile("720p_3");
      this.subscribeToStreams();
    });
  }

  private subscribeToStreams() {
    this.localStream.on("accessAllowed", () => {
      console.log("accessAllowed");
    });
    // The user has denied access to the camera and mic.
    this.localStream.on("accessDenied", () => {
      console.log("accessDenied");
    });

    this.localStream.init(
      () => {
        console.log("getUserMedia successfully");
        this.localStream.play("agora_local");
        this.agoraService.client.publish(this.localStream, function(err) {
          console.log("Publish local stream error: " + err);
        });
        this.agoraService.client.on("stream-published", function(evt) {
          console.log("Publish local stream successfully");
          console.log(new Date().getTime());
        });
      },
      function(err) {
        console.log("getUserMedia failed", err);
      }
    );

    this.agoraService.client.configPublisher({
      width: "500",
      height: "500",
      framerate: "15",
      bitrate: "500"
      // publishUrl: "rtmp://xxx/xxx/"
    });

    // Add
    this.agoraService.client.on("error", err => {
      console.log("Got error msg:", err.reason);
      if (err.reason === "DYNAMIC_KEY_TIMEOUT") {
        this.agoraService.client.renewChannelKey(
          "",
          () => {
            console.log("Renew channel key successfully");
          },
          err => {
            console.log("Renew channel key failed: ", err);
          }
        );
      }
    });

    // Add
    this.agoraService.client.on("stream-added", evt => {
      const stream = evt.stream;
      this.agoraService.client.subscribe(stream, err => {
        if (err) {
          console.log("Subscribe stream failed", err);
        }
      });
    });

    // Add
    this.agoraService.client.on("stream-subscribed", evt => {
      const stream = evt.stream;
      if (!this.remoteCalls.includes(`agora_remote${stream.getId()}`))
        this.remoteCalls.push(`agora_remote${stream.getId()}`);
      setTimeout(() => stream.play(`agora_remote${stream.getId()}`), 2000);
      console.log("sssss", new Date().getTime());
    });

    // Add
    this.agoraService.client.on("stream-removed", evt => {
      const stream = evt.stream;
      console.log("this is stream data ---------", stream);
      stream.stop();
      this.remoteCalls = this.remoteCalls.filter(
        call => call !== `#agora_remote${stream.getId()}`
      );
      console.log(`Remote stream is removed ${stream.getId()}`);
      console.log("svvvvv", new Date().getTime());
    });

    // Add
    this.agoraService.client.on("peer-leave", evt => {
      const stream = evt.stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(
          call => call === `#agora_remote${stream.getId()}`
        );
        console.log(`${evt.uid} left from this channel`);
      }
    });
  }

  endCall() {
    this.agoraService.client.unsubscribe(this.localStream, function(err, data) {
      console.log("both stream ended");
    });
    this.localStream.stop();
    this.localStream.close();
    console.log("aaasss", new Date().getTime());
  }
}
