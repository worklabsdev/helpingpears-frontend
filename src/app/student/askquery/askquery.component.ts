import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../providers/student.service';
import {DomSanitizer}  from '@angular/platform-browser';
import {MessageService} from 'primeng/api';
import {WebcamImage} from 'ngx-webcam';
//imported here just for type checking. Optional

@Component({
  selector: 'app-askquery',
  templateUrl: './askquery.component.html',
  styleUrls: ['./askquery.component.css']
})
export class AskqueryComponent implements OnInit {
  question:any;
  queryData:any;
  title:any;
  showForm:Boolean =false;
  public webcamImage: WebcamImage = null;
  clickPicture:Boolean = false;
  constructor(public studentService:StudentService,public sanetizer:DomSanitizer, public messageService : MessageService) { }

  ngOnInit() {
    this.getqueries();
  }


  shForm(){
    this.showForm=  !this.showForm;
  }

  
  handleImage(webcamImage: WebcamImage) {
    this.webcamImage = webcamImage;
     console.log("getting image",this.webcamImage.imageAsDataUrl);
  }
  getqueries() {
    var studentId = localStorage.user_id;
    this.studentService.getqueries(studentId).subscribe((data:any)=>{
      console.log("this is the data",data.data);
      if(data.data.length){
        this.queryData = data.data;
        for(var t=0;t<this.queryData.length;t++){
          this.queryData[t].question = this.sanetizer.bypassSecurityTrustUrl(this.queryData[t].question);
        }
        console.log("thisi s ht equery Data",this.queryData);
      }
      else {
        this.queryData = [];
      }
      // console.log("this is query data",this.queryData[0].query);

    },(error:any)=>{
      console.log("thisis the error",error);
    })
  }

  savequery() {
    var data = {};
    
    if(this.webcamImage){
    data = {
      email:localStorage.getItem('user_email'),
      title:this.title,
      question : this.question,
      studentId : localStorage.user_id,
      queryImage: this.webcamImage.imageAsDataUrl
    }
    }
    else {
       data = {
        email:localStorage.getItem('user_email'),
        title:this.title,
        question : this.question,
        studentId : localStorage.user_id
      }
    }
    console.log("this is data",data);
    this.studentService.savequery(data).subscribe((data:any)=>{
      console.log("this is the data",data);
      this.messageService.add({severity:'success', summary:'Query Created'});
      this.showForm=false;
      this.getqueries();
    },(error:any)=>{
      console.log("this is the error",error);
    })
  }


}
