import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginenableComponent } from './loginenable.component';

describe('LoginenableComponent', () => {
  let component: LoginenableComponent;
  let fixture: ComponentFixture<LoginenableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginenableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginenableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
