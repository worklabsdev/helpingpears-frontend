import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { StudentService } from '../../providers/student.service';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  studentdata:any;
  title = 'hashlearn';
  modalRef: BsModalRef;
  optionsList:any=[];
  msgs: Message[] = [];
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  constructor(private modalService: BsModalService,public studentservice:StudentService) {}

  ngOnInit() {
    this.dropdownList = [
      { id: 1, lang_text: 'Albanian' },
      { id: 2, lang_text: 'Ancient Greek' },
      { id: 3, lang_text: 'Catalan' },
      { id: 4, lang_text: 'Esperanto' },
      { id: 5, lang_text: 'French' },
      { id: 6, lang_text: 'Galician' },
      { id: 7, lang_text: 'Hebrew' },
      { id: 8, lang_text: 'Hiligaynon' },
      { id: 9, lang_text: 'Hungarian' },
      { id: 10, lang_text: 'Icelandic' },
      { id: 11, lang_text: 'Ido' },
      { id: 12, lang_text: 'Irish' },
      { id: 13, lang_text: 'Italian' },
      { id: 14, lang_text: 'Kashubian' },
      { id: 15, lang_text: 'Latin' },
      { id: 16, lang_text: 'Lithuanian' },
      { id: 17, lang_text: 'Lower Sorbian' },
      { id: 18, lang_text: 'Mapudungun' },
      { id: 19, lang_text: 'Old French' },
      { id: 20, lang_text: 'Polish' },
      { id: 21, lang_text: 'Portuguese' },
      { id: 22, lang_text: 'Romanian' },
      { id: 23, lang_text: 'Russian' },
      { id: 24, lang_text: 'Spanish' },
      { id: 25, lang_text: 'Upper Sorbian' }
    ];
  this.selectedItems = [
    { item_id: 3, item_text: 'Pune' },
    { item_id: 4, item_text: 'Navsari' }
  ];
  this.dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'lang_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
    var userid = localStorage.user_id;
    this.getstudentdata(userid);

    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: 'item_id',
    //   textField: 'item_text',
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'UnSelect All',
    //   itemsShowLimit: 3,
    //   allowSearchFilter: true
    // };


  }


  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }



  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'gray modal-lg' })
    );
  };

  // education = ['Prof'];
  // skills = ['Physics'];

  getstudentdata(userid){
    this.studentservice.getstudentProfileData(userid).subscribe((data:any)=>{
      console.log("this is the data",data);
      this.studentdata = data.data[0];
      console.log("this is student data",this.studentdata);
    },(error:any)=>{
      console.log("this si the error",error);
    })
  }

  editprofile() {
    var edata =  {
      firstname:this.studentdata.firstname,
      class:this.studentdata.class,
      age:this.studentdata.age,
      language:this.studentdata.languages,
      about:this.studentdata.about
    }
    var studentid =  localStorage.user_id
    this.studentservice.editprofile(studentid,edata).subscribe((data:any)=>{
      this.modalRef.hide()
      console.log("this is the data updated",data);
    },(error:any)=>{
      console.log("this is the error ",error);
    })
    console.log("this is the changed data",edata);
  }
}
