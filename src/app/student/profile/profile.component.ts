import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../providers/student.service';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
studentdata:any;
  constructor(public studentservice:StudentService) { }

  ngOnInit() {
      var userid = localStorage.user_id;
      this.getstudentdata(userid);
  }

  getstudentdata(userid){

    this.studentservice.getstudentProfileData(userid).subscribe((data:any)=>{
      console.log("this is the datrererea",data);
      this.studentdata = data.data[0];
    },(error:any)=>{
      console.log("this si the error",error);
    })
  }

}
