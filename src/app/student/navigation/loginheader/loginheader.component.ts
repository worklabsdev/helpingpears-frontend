import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { StudentService } from "../../../providers/student.service";
import { GlobalService } from "../../../providers/global.service";
import { RouterModule, Router } from "@angular/router";
import { Message } from "primeng/api";
import { MessageService } from "primeng/components/common/messageservice";

import { environment } from "../../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-loginheader",
  templateUrl: "./loginheader.component.html",
  styleUrls: ["./loginheader.component.css"]
})
export class LoginheaderComponent implements OnInit {
  name: string;
  username: string;
  dpUrl: String;
  studentData: any;
  dp_64textString: String;
  constructor(
    public globalService: GlobalService,
    public studentService: StudentService,
    public router: Router
  ) {
    this.dpUrl = APIEndpoint + "public/student/dp/";
  }

  ngOnInit() {
    // this.name = localStorage.name;
    this.name = localStorage.user_name;
    console.log("this is the name first", this.name);
    this.getstudentdata();
  }

  logout() {
    // this.logged_id =0;
    localStorage.clear();
    this.router.navigate(["/"]);
  }

  getstudentdata() {
    var userid = localStorage.user_id;
    this.studentService.getstudentProfileData(userid).subscribe(
      (data: any) => {
        console.log("this is the data of user", data);
        this.studentData = data.data[0];
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }
}
