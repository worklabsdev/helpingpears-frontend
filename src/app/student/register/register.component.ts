import { Component, OnInit } from "@angular/core";
import { Router, RouterModule } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
// import { StudentInterface } from './studentInterface';
import { Message } from "primeng/components/common/api";

import { MessageService } from "primeng/components/common/messageservice";
import { StudentService } from "../../providers/student.service";
import { IfStmt } from "@angular/compiler";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.css"]
})
export class RegisterComponent implements OnInit {
  name: string;
  email: string;
  password: string;
  authemail: string;
  authpass: string;
  regLoading: Boolean = false;
  msgs: Message[] = [];
  regForm: FormGroup;
  constructor(
    public studentService: StudentService,
    public router: Router,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.regForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      age: new FormControl("", [
        Validators.required,
        Validators.pattern("^[0-9]*$")
      ]),
      class: new FormControl("", [Validators.required]),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(7)
      ])
    });
  }

  checkStudentEmail() {
    // Email Unique Check
    var email = this.regForm.value.email.trim();

    if (!email) return;

    this.studentService.checkStudentEmail(email).subscribe(
      (result: any) => {
        if (!result.data.unique) {
          this.messageService.add({
            severity: "error",
            summary: "Try another email",
            detail: result.message
          });
        }
      },
      (error: any) => {
        error._body = JSON.parse(error._body);

        this.messageService.add({
          severity: "error",
          summary: "Error",
          detail: error._body.message || error.statusText
        });
      }
    );
  } // Email Unique Check

  register() {
    if (!this.regForm.valid) return;

    this.regLoading = true;

    this.studentService
      .register({
        firstname: this.regForm.value.name,
        email: this.regForm.value.email,
        age: this.regForm.value.age,
        password: this.regForm.value.password,
        class: this.regForm.value.class
      })
      .subscribe(
        (result: any) => {
          console.log("this is datarrrrraraa", result);

          this.regLoading = false;

          localStorage.setItem("token", result.data.token);
          localStorage.setItem("user_name", result.data.userdata.name);
          localStorage.setItem("user_email", result.data.userdata.email);
          localStorage.setItem("user_id", result.data.userdata._id);
          localStorage.setItem("type", "student");

          this.regForm.reset();

          setTimeout(() => {
            this.router.navigate(["eprofile"]);
            this.addcard();
          }, 3000);
        },
        (error: any) => {
          this.regLoading = false;

          console.error(
            "Register Api Error --------",
            error,
            "-------- Register Api Error"
          );

          error._body = JSON.parse(error._body);

          this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: error._body.message || error.statusText
          });
        }
      );
  }

  addcard() {
    // localStorage.removeItem('totalTimeCall');
    var body = {
      email: localStorage.getItem("user_email"),
      amount: "$1.00"
    };
    this.studentService.addcard(body).subscribe(
      (data: any) => {
        console.log("thisi s the paymentttttttttt", data);
      },
      (error: any) => {
        console.log("this is error", error);
      }
    );
  }
}
