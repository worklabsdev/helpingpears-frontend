import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TContactAdminComponent } from './t-contact-admin.component';

describe('TContactAdminComponent', () => {
  let component: TContactAdminComponent;
  let fixture: ComponentFixture<TContactAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TContactAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TContactAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
