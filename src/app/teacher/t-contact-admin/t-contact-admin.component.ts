import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../providers/global.service';
import { TeacherService } from '../../providers/teacher.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-t-contact-admin',
  templateUrl: './t-contact-admin.component.html',
  styleUrls: ['./t-contact-admin.component.css']
})
export class TContactAdminComponent implements OnInit {
  querySentLoading:Boolean=false;
  qryAdminData:any;
  noDataFlag:Boolean = false;
  qryForm : FormGroup;
  msgs:Message[]=[];
  constructor(public globalService:GlobalService,public teacherService:TeacherService,private messageService: MessageService) { }

  ngOnInit() {
    this.qryForm = new FormGroup({
      title: new FormControl('',Validators.required),
      question :new FormControl('',[Validators.required])
    })
    this.getTeacherAdminQueries();
  }
  getTeacherAdminQueries(){
      var studentid = localStorage.user_id;
      this.teacherService.getTeacherAdminQueries(studentid).subscribe((data:any)=>{
        console.log("this is the datasdfasd",data);
        if(data.status==200){
          this.qryAdminData = data.data;
        }else{
          this.noDataFlag = true;
        }

      },(error:any)=>{
        console.log("this is the error",error);
      })

  }

  askAdmin(){
    if(this.qryForm.valid){
      this.querySentLoading=true;
      var studentid = localStorage.user_id;
      var postData = {
        title:this.qryForm.value.title,
        question:this.qryForm.value.question,
        sender: {
          senderId : studentid
        }
      }
      this.teacherService.askQueryToAdmin(postData).subscribe((data:any)=>{
        if(data) {
          this.qryForm.reset();
          this.getTeacherAdminQueries();
          this.querySentLoading=false;
          this.messageService.add({severity:'success', summary:'Query Sent', detail:'Query Sent to Admin. Will get back to you with in 24 hours'});
        }
        console.log("this is the data",data);
      },(error:any)=>{
        console.log("this is the error ",error);
      })
    }
  }

}
