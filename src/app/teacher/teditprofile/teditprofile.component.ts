import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { TeacherService } from "../../providers/teacher.service";
import { GlobalService } from "../../providers/global.service";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";
import { HttpModule, Http } from "@angular/http";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-teditprofile",
  templateUrl: "./teditprofile.component.html",
  styleUrls: ["./teditprofile.component.css"]
})
export class TeditprofileComponent implements OnInit {
  teacherData: any;
  title = "hashlearn";
  modalRef: BsModalRef;
  optionsList: any = [];
  msgs: Message[] = [];
  languageList = [];
  selectedItems = [];
  languageDropdownSettings = {};
  subjectList = [];
  subjectDropdownSettings = {};
  doc_64textString: String;
  docImageUrl: String;
  teacherimage: any;
  profileComplete: Boolean = false;
  profilesendingLoading: Boolean = false;
  stripeToken: any;
  constructor(
    private http: Http,
    private modalService: BsModalService,
    public teacherservice: TeacherService,
    public globalservice: GlobalService,
    private messageService: MessageService
  ) {
    this.docImageUrl = APIEndpoint + "public/teacher/docs/";
    this.teacherimage = this.globalservice.getTeacher().subscribe(message => {
      this.getteacherdata();
    });
  }

  ngOnInit() {
    this.languageList = this.globalservice.languages;
    this.languageDropdownSettings = this.globalservice.langsettings;
    this.subjectList = this.globalservice.subjects;
    this.subjectDropdownSettings = this.globalservice.subjectsetting;
    var userid = localStorage.user_id;
    this.getteacherdata();

    // this.dropdownSettings = {
    //   singleSelection: false,
    //   idField: 'item_id',
    //   textField: 'item_text',
    //   selectAllText: 'Select All',
    //   unSelectAllText: 'UnSelect All',
    //   itemsShowLimit: 3,
    //   allowSearchFilter: true
    // };
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  openModalWithClass(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "gray modal-lg" })
    );
  }

  // education = ['Prof'];
  // skills = ['Physics'];

  getteacherdata() {
    let id = localStorage.user_id;
    this.teacherservice.getteacherProfileData(id).subscribe(
      (data: any) => {
        console.log("this is the data", data);
        this.teacherData = data.data[0];
        if (
          this.teacherData.profileImage &&
          this.teacherData.title &&
          this.teacherData.subjects[0] &&
          this.teacherData.languages &&
          this.teacherData.about &&
          this.teacherData.age &&
          this.teacherData.docs.name &&
          (!this.teacherData.docs.isDegreeApproved ||
            this.teacherData.docs.isDegreeApproved == "approved" ||
            this.teacherData.docs.isDegreeApproved == "") &&
          this.teacherData.emailverified
        ) {
          this.profileComplete = true;
        }
        console.log("this is student data", this.teacherData);
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }
  sendProfileForReview() {
    if (this.profileComplete) {
      this.profilesendingLoading = true;
      let id = localStorage.user_id;
      this.teacherservice.sendTeacherProfileForReview(id).subscribe(
        (data: any) => {
          if (data.status == 200) {
            this.getteacherdata();
            this.profilesendingLoading = false;
            this.messageService.add({
              severity: "success",
              summary: "Mail Sent",
              detail:
                "Thank you for completing you profile. Please wait while Admin examin your profile"
            });
          }
          console.log("this is the data from teachers ", data);
        },
        (error: any) => {
          console.log("this is the error");
        }
      );
    } else {
      // setTimeout(()=>{
      this.messageService.add({
        severity: "error",
        summary: "Complete Profile",
        detail:
          "Please complete all the points mentioned above, before sending profile for approval"
      });
      // },3000)
    }
  }

  async openCheckout() {
    var handler = (<any>window).StripeCheckout.configure({
      key: "pk_test_GeC9styUFdtFxKJ2WmnNs6OJ",
      locale: "auto",
      token: await function(token: any) {
        this.stripeToken = token.id;
        console.log("this is stripe token", token.id);
        // You can access the token ID with `token.id`.
        // Get the token ID to your server-side code for use.
        if (token.id) {
          var httpreq = this.http.get(
            APIEndpoint + "t/createuserstripe/" + this.stripeToken
          );
          console.log("this is requesdt", httpreq);
        }
      }
      //  this.http.get(APIEndpoint + 't/createuserstripe/'+ this.stripeToken)
    });
    // this.teacherservice.CreateUserStripe(this.stripeToken).subscribe((data:any)=>{
    //     console.log("data from stripe",data);
    // },(error:any)=>{
    //   console.log("error",error);
    // })
    handler.open({
      name: "Helping Pears",
      description: "Please add your card for ",
      amount: 100
    });
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString = btoa(binaryString);
    this.doc_64textString = "data:image/png;base64," + this.doc_64textString;
    // console.log(btoa(binaryString));
  }

  uploaddoc() {
    if (this.doc_64textString) {
      var postData = {
        docs: this.doc_64textString
      };
      var id = localStorage.user_id;
      this.teacherservice.uploaddoc(id, postData).subscribe(
        (data: any) => {
          if (data) {
            console.log("uploaded docs", data);
            var userid = localStorage.user_id;
            this.doc_64textString = "";
            this.messageService.add({
              severity: "success",
              summary: "Admin will soon review your degree for approval",
              detail: "Uploaded Successfully"
            });
            this.getteacherdata();
          }
        },
        (error: any) => {
          console.log("this is error", error);
        }
      );
    } else {
      return false;
    }
  }

  editprofile() {
    var edata = {
      name: this.teacherData.name,
      age: this.teacherData.age,
      title: this.teacherData.title,
      language: this.teacherData.languages,
      subjects: this.teacherData.subjects,
      about: this.teacherData.about
    };
    var id = localStorage.user_id;
    this.teacherservice.editprofile(id, edata).subscribe(
      (data: any) => {
        // document.getElementById('btnclose').click();
        this.modalRef.hide();
        console.log("this is the data updated", data);
      },
      (error: any) => {
        console.log("this is the error ", error);
      }
    );
    console.log("this is the changed data", edata);
  }
}
