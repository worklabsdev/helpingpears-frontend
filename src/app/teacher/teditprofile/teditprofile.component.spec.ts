import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeditprofileComponent } from './teditprofile.component';

describe('TeditprofileComponent', () => {
  let component: TeditprofileComponent;
  let fixture: ComponentFixture<TeditprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeditprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeditprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
