import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TMyJobsComponent } from './t-my-jobs.component';

describe('TMyJobsComponent', () => {
  let component: TMyJobsComponent;
  let fixture: ComponentFixture<TMyJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TMyJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TMyJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
