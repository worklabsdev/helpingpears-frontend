import { Component, OnInit } from "@angular/core";
import { TeacherService } from "../../providers/teacher.service";
import { GlobalService } from "../../providers/global.service";
import { DomSanitizer } from "@angular/platform-browser";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-t-my-jobs",
  templateUrl: "./t-my-jobs.component.html",
  styleUrls: ["./t-my-jobs.component.css"]
})
export class TMyJobsComponent implements OnInit {
  queryData: any;
  dpUrl: any;
  formRating: any = 4.5;
  msgs: Message[] = [];
  constructor(
    public teacherService: TeacherService,
    public sanetizer: DomSanitizer,
    public globalService: GlobalService
  ) {}

  ngOnInit() {
    this.getMyJobs();
    this.dpUrl = APIEndpoint + "public/student/dp/";
  }

  getMyJobs() {
    var id = localStorage.user_id;
    this.teacherService.getMyJobs(id).subscribe(
      (data: any) => {
        this.queryData = data.data;
        for (var t = 0; t < this.queryData.length; t++) {
          this.queryData[t].question = this.sanetizer.bypassSecurityTrustUrl(
            this.queryData[t].question
          );
          console.log("my jobs are ", this.queryData);
        }
      },
      (error: any) => {
        console.log("this is the error", error);
      }
    );
  }
}
