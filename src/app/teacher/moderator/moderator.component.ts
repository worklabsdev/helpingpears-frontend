import { Component, OnInit ,AfterViewInit,ViewChild,ElementRef,Input} from '@angular/core';
import { OpentokService } from '../../providers/opentok.service';

const publish = () => {

};
@Component({
  selector: 'app-moderator',
  templateUrl: './moderator.component.html',
  styleUrls: ['./moderator.component.css']
})
export class ModeratorComponent implements  AfterViewInit{
  @ViewChild('publisherDiv') publisherDiv: ElementRef;
   @Input() session: OT.Session;
   publisher: OT.Publisher;
   publishing: Boolean;
   constructor(private opentokService: OpentokService) {
     this.publishing = false;
   }
   ngAfterViewInit() {
   const OT = this.opentokService.getOT();
   this.publisher = OT.initPublisher(this.publisherDiv.nativeElement, {insertMode: 'append'});

   if (this.session) {
     if (this.session['isConnected']()) {
       this.publish();
     }
     this.session.on('sessionConnected', () => this.publish());
   }
 }

 publish() {
  this.session.publish(this.publisher, (err) => {
    if (err) {
      alert(err.message);
    } else {
      this.publishing = true;
    }
  });
}

disconnectcall() {
  this.publisher.destroy();
  // alert('hey');
  // this.session.forceDisconnect
  // this.session.disconnect(OT.Connection);
  // this.session.forceDisconnect(OT.Connection,funciton(err){
  //   if(err) console.log("error",err);
  // });
}

}
