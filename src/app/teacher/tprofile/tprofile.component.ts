import { Component, OnInit } from "@angular/core";
import { TeacherService } from "../../providers/teacher.service";
import { GlobalService } from "../../providers/global.service";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-tprofile",
  templateUrl: "./tprofile.component.html",
  styleUrls: ["./tprofile.component.css"]
})
export class TprofileComponent implements OnInit {
  teacherData: any;
  dpUrl: any;
  constructor(
    public teacherservice: TeacherService,
    public globalService: GlobalService
  ) {}

  ngOnInit() {
    var userid = localStorage.user_id;
    this.getteacherdata(userid);
    this.dpUrl = APIEndpoint + "public/teacher/dp/";
  }

  getteacherdata(userid) {
    this.teacherservice.getteacherProfileData(userid).subscribe(
      (data: any) => {
        console.log("this is the teacher data", data);
        this.teacherData = data.data[0];
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }
}
