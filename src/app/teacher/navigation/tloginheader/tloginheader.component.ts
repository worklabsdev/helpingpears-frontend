import { Component, OnInit } from "@angular/core";
import { TeacherService } from "../../../providers/teacher.service";
import { RouterModule, Router } from "@angular/router";
import { GlobalService } from "src/app/providers/global.service";

import { environment } from "../../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-tloginheader",
  templateUrl: "./tloginheader.component.html",
  styleUrls: ["./tloginheader.component.css"]
})
export class TloginheaderComponent implements OnInit {
  name: string;
  username: string;
  teacherData: any;
  dpUrl: String;
  constructor(
    public teacherservice: TeacherService,
    public globalService: GlobalService,
    public router: Router
  ) {
    this.dpUrl = APIEndpoint + "public/teacher/dp/";
  }

  ngOnInit() {
    // this.name = localStorage.name;
    this.name = localStorage.user_name;
    console.log("this is the name first", this.name);
    // getstudentdata()
    var userid = localStorage.user_id;
    this.getteacherdata(userid);
  }

  logout() {
    // this.logged_id =0;
    console.log("you are logging out ");
    localStorage.clear();
    console.log("you are logging out ", localStorage.user_name);
    this.router.navigate(["/"]);
  }
  getteacherdata(userid) {
    this.teacherservice.getteacherProfileData(userid).subscribe(
      (data: any) => {
        console.log("this is the data", data);
        this.teacherData = data.data[0];
        console.log("this is student data", this.teacherData);
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }
}
