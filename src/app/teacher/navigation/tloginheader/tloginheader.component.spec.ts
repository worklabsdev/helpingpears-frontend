import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TloginheaderComponent } from './tloginheader.component';

describe('TloginheaderComponent', () => {
  let component: TloginheaderComponent;
  let fixture: ComponentFixture<TloginheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TloginheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TloginheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
