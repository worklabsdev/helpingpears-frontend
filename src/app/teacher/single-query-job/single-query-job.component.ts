import { Component, OnInit, ElementRef, ViewChild, Input } from "@angular/core";
import { GlobalService } from "../../providers/global.service";
import { StudentService } from "../../providers/student.service";
import { TeacherService } from "../../providers/teacher.service";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";
// import * as OT from '@opentok/client';
// import { OpentokService } from '../../providers/opentok.service';
import { AngularAgoraRtcService, Stream } from "angular-agora-rtc";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

declare function join(): any;
declare function leave(): any;
// declare function getDevices():any;
declare function detect(): any;
declare var formComponent2: any;

@Component({
  selector: "app-single-query-job",
  templateUrl: "./single-query-job.component.html",
  styleUrls: ["./single-query-job.component.css"]
})
export class SingleQueryJobComponent implements OnInit {
  queryData: any;
  slug: any;
  qryForm: FormGroup;
  ratingForm: FormGroup;
  msgs: Message[] = [];
  dpUrl: any;
  title = "AgoraDemo";
  localStream: Stream;
  remoteCalls: any = []; // Add
  // session: OT.Session;
  querySentLoading: any;
  token: string;
  teacherData: any;

  constructor(
    public studentService: StudentService,
    public sanetizer: DomSanitizer,
    public activatedRoute: ActivatedRoute,
    public teacherService: TeacherService,
    public globalService: GlobalService,
    private messageService: MessageService,
    private agoraService: AngularAgoraRtcService
  ) {
    this.agoraService.createClient();
    this.slug = this.activatedRoute.snapshot.params.slug;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
    formComponent2 = this;

    (<HTMLInputElement>document.getElementById("channel")).value = Math.floor(
      Math.random() * 100000000
    ).toString();
    this.ratingForm = new FormGroup({
      rating: new FormControl("", [Validators.required]),
      comment: new FormControl("", [Validators.required])
    });
    this.qryForm = new FormGroup({
      replyChat: new FormControl("", [Validators.required])
    });
    this.dpUrl = APIEndpoint + "public/student/dp/";
    this.getSingleStudentsQueries();
    this.getSingleTeacher();
  }

  ngOnDestroy(): any {
    formComponent2 = null;
  }

  join() {
    join();
  }

  leave() {
    leave();
  }

  startcall() {
    this.agoraService.client.join(null, "1000", null, uid => {
      this.localStream = this.agoraService.createStream(
        uid,
        true,
        null,
        null,
        true,
        false
      );
      this.localStream.setVideoProfile("720p_3");
      this.subscribeToStreams();
    });
  }

  private subscribeToStreams() {
    this.localStream.on("accessAllowed", () => {
      console.log("accessAllowed");
    });
    // The user has denied access to the camera and mic.
    this.localStream.on("accessDenied", () => {
      console.log("accessDenied");
    });

    this.localStream.init(
      () => {
        console.log("getUserMedia successfully");
        this.localStream.play("agora_local");
        this.agoraService.client.publish(this.localStream, function(err) {
          console.log("Publish local stream error: " + err);
        });
        this.agoraService.client.on("stream-published", function(evt) {
          console.log("Publish local stream successfully");
          console.log(new Date().getTime());
        });
      },
      function(err) {
        console.log("getUserMedia failed", err);
      }
    );

    this.agoraService.client.configPublisher({
      width: "500",
      height: "500",
      framerate: "15",
      bitrate: "500"
      // publishUrl: "rtmp://xxx/xxx/"
    });

    // Add
    this.agoraService.client.on("error", err => {
      console.log("Got error msg:", err.reason);
      if (err.reason === "DYNAMIC_KEY_TIMEOUT") {
        this.agoraService.client.renewChannelKey(
          "",
          () => {
            console.log("Renew channel key successfully");
          },
          err => {
            console.log("Renew channel key failed: ", err);
          }
        );
      }
    });

    // Add
    this.agoraService.client.on("stream-added", evt => {
      const stream = evt.stream;
      this.agoraService.client.subscribe(stream, err => {
        if (err) {
          console.log("Subscribe stream failed", err);
        }
      });
    });

    // Add
    this.agoraService.client.on("stream-subscribed", evt => {
      const stream = evt.stream;
      if (!this.remoteCalls.includes(`agora_remote${stream.getId()}`))
        this.remoteCalls.push(`agora_remote${stream.getId()}`);
      setTimeout(() => stream.play(`agora_remote${stream.getId()}`), 2000);
      console.log("sssss", new Date().getTime());
    });

    // Add
    this.agoraService.client.on("stream-removed", evt => {
      const stream = evt.stream;
      console.log("this is stream data ---------", stream);
      stream.stop();
      this.remoteCalls = this.remoteCalls.filter(
        call => call !== `#agora_remote${stream.getId()}`
      );
      console.log(`Remote stream is removed ${stream.getId()}`);
      console.log("svvvvv", new Date().getTime());
    });

    // Add
    this.agoraService.client.on("peer-leave", evt => {
      const stream = evt.stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(
          call => call === `#agora_remote${stream.getId()}`
        );
        console.log(`${evt.uid} left from this channel`);
      }
    });
  }

  endCall() {
    this.agoraService.client.unsubscribe(this.localStream, function(err, data) {
      console.log("both stream ended");
    });
    this.localStream.stop();
    this.localStream.close();
    console.log("aaasss", new Date().getTime());
  }

  getSingleStudentsQueries() {
    this.studentService.getsinglequery(this.slug).subscribe(
      (data: any) => {
        console.log("thisi s the data", data);
        this.queryData = data.data;
        console.log("this is question Data", this.queryData);
        // for(var t=0;t<this.queryData.length;t++){
        this.queryData.question = this.sanetizer.bypassSecurityTrustUrl(
          this.queryData.question
        );
        // }
      },
      (error: any) => {
        console.log("this is error", error);
      }
    );
  }
  getSingleTeacher() {
    var id = localStorage.user_id;
    this.teacherService.getteacherProfileData(id).subscribe(
      (data: any) => {
        console.log("tnihs teacherszdfis data", data);
        this.teacherData = data.data[0];
        // getDevices();
        detect();
      },
      (error: any) => {
        console.log("thisi si error", error);
        // getDevices();
        detect();
      }
    );
  }
  applyForJob() {
    var postData = {
      selectedteacher: localStorage.user_id
    };
    this.teacherService.applyForJob(this.slug, postData).subscribe(
      (data: any) => {
        console.log("this is data", data);

        this.messageService.add({
          severity: "success",
          summary: "Applied Successfully",
          detail: "Thank you applying for the job"
        });
        this.getSingleStudentsQueries();
      },
      (error: any) => {
        console.log("this is error", error);
      }
    );
  }
  rateStudent() {
    if (this.ratingForm.valid) {
      var postData = {
        rating: this.ratingForm.value.rating,
        comment: this.ratingForm.value.comment
      };
      this.studentService.rateStudent(this.slug, postData).subscribe(
        (data: any) => {
          console.log("this is ithe data", data);
        },
        (error: any) => {
          console.log("this is error", error);
        }
      );
    }
  }
  disconnect() {
    // this.opentokService.disconnect();
  }
  messageToStudent() {
    if (this.qryForm.valid) {
      var postData = {
        userType: "teacher",
        replyText: this.qryForm.value.replyChat,
        repliedAt: new Date()
      };
      var slug = this.activatedRoute.snapshot.paramMap.get("slug");
      this.teacherService.messageToStudent(slug, postData).subscribe(
        (data: any) => {
          this.qryForm.reset();
          this.getSingleStudentsQueries();
        },
        (error: any) => {
          console.log("this is error", error);
        }
      );
    }
  }

  payment(time) {
    // console.log("temailidddd",this.queryData.studentId.email);
    // var body={
    //   email:this.queryData.studentId.email,
    //   amount:time*80
    // }
    // this.studentService.chargepayment(body).subscribe((data:any)=>{
    //   console.log("thisi s the paymentttttttttt",data);

    // },(error:any)=>{
    //   console.log("this is error",error);
    // })

    var body = {
      email: this.queryData.studentId.email,
      amount: time * 80,
      status: true,
      id: this.queryData.studentId._id
    };
    this.studentService.paymentStatusset(body).subscribe(
      (data: any) => {
        console.log("thisi s the paymentttttttttt", data);
        if (data.message == "false") {
          this.studentService.chargepayment(body).subscribe(
            (data: any) => {
              console.log("thisi s the paymentttttttttt", data);
            },
            (error: any) => {
              console.log("this is error", error);
              // timeConsumed();
            }
          );
        }
      },
      (error: any) => {
        console.log("this is error", error);
        // timeConsumed();
      }
    );
  }

  startclass() {
    // alert('hau');
    // this.teacherService.startclass(this.slug).subscribe((data:any)=>{
    //   console.log("data",data)
    //   this.session = this.opentokService.getOT().initSession('61d5bcd0dfaafe5913cb3b28b3b53177a2fd8cbb', data.sessionId);
    //   this.token = data.token_ops;
    //   return Promise.resolve(this.session);
    // },(error:any)=>{
    //   console.log("error",error);
    // })
  }

  //    connect() {
  //   return new Promise((resolve, reject) => {
  //     this.session.connect(this.token, (err) => {
  //       if (err) {
  //         reject(err);
  //       } else {
  //         resolve(this.session);
  //       }
  //     });
  //   });
  // }
}
