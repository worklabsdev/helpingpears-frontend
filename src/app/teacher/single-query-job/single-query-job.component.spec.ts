import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleQueryJobComponent } from './single-query-job.component';

describe('SingleQueryJobComponent', () => {
  let component: SingleQueryJobComponent;
  let fixture: ComponentFixture<SingleQueryJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleQueryJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleQueryJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
