import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../providers/global.service';
import { StudentService } from '../../providers/student.service';
import { TeacherService } from '../../providers/teacher.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { ActivatedRoute,RouterModule,Router} from  '@angular/router';
@Component({
  selector: 'app-ts-admin-query',
  templateUrl: './ts-admin-query.component.html',
  styleUrls: ['./ts-admin-query.component.css']
})
export class TsAdminQueryComponent implements OnInit {
  querySentLoading:Boolean=false;
  qryAdminData:any;
  noDataFlag:Boolean = false;
  qryForm : FormGroup;
  msgs:Message[]=[];
  constructor(public globalService:GlobalService,public studentService:StudentService
    ,private messageService: MessageService,public activatedRoute:ActivatedRoute,public teacherService:TeacherService) { }

    ngOnInit() {
      this.qryForm = new FormGroup({
        replytext :new FormControl('',[Validators.required])
      })
      this.getSingleAdminQuery();
    }

    getSingleAdminQuery() {
      var studentid = this.activatedRoute.snapshot.paramMap.get('id');
      this.teacherService.getSingleAdminQuery(studentid).subscribe((data:any)=>{
        console.log("this is the datasdfasd",data);
        if(data.status==200){
          this.qryAdminData = data.data;
        }else{
          this.noDataFlag = true;
        }
      },(error:any)=>{
        console.log("this is the error",error);
      })
    }

    teacherReply() {
      if(this.qryForm.valid){
        var postData = {
          userType : 'teacher',
          replyText: this.qryForm.value.replytext,
          repliedAt : new Date()
        }
        var queryid = this.activatedRoute.snapshot.paramMap.get('id');
        this.teacherService.teacherReply(queryid,postData).subscribe((data:any)=>{
          this.qryForm.reset();
          this.getSingleAdminQuery();
        },(error:any)=>{
          console.log("this is error",error);
        })
      }
    }


  }
