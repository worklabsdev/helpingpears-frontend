import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TsAdminQueryComponent } from './ts-admin-query.component';

describe('TsAdminQueryComponent', () => {
  let component: TsAdminQueryComponent;
  let fixture: ComponentFixture<TsAdminQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TsAdminQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TsAdminQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
