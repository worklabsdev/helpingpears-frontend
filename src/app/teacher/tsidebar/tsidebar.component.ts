import { Component, OnInit } from "@angular/core";
import { GlobalService } from "../../providers/global.service";
import { TeacherService } from "../../providers/teacher.service";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-tsidebar",
  templateUrl: "./tsidebar.component.html",
  styleUrls: ["./tsidebar.component.css"]
})
export class TsidebarComponent implements OnInit {
  doc_64textString: String;
  dpUrl: String;
  teacherData: any;
  constructor(
    public teacherService: TeacherService,
    public globalService: GlobalService,
    private messageService: MessageService
  ) {
    this.dpUrl = APIEndpoint + "public/teacher/dp/";
  }

  ngOnInit() {
    var userid = localStorage.user_id;
    this.getteacherdata(userid);
  }

  getteacherdata(userid) {
    this.teacherService.getteacherProfileData(userid).subscribe(
      (data: any) => {
        console.log("this is the data", data);
        this.teacherData = data.data[0];
        console.log("this is student data", this.teacherData);
      },
      (error: any) => {
        console.log("this si the error", error);
      }
    );
  }

  removeDp() {
    var id = localStorage.user_id;
    this.teacherService.removeprofileImage(id).subscribe(
      (data: any) => {
        console.log("this is the image removed", data);
        this.getteacherdata(localStorage.user_id);
      },
      (error: any) => {
        console.log("this is the error", error);
      }
    );
  }

  handleFileSelect(evt) {
    var files = evt.target.files;
    var file = files[0];
    if (files && file) {
      var reader = new FileReader();
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.doc_64textString = btoa(binaryString);
    this.doc_64textString = "data:image/png;base64," + this.doc_64textString;
    // console.log(btoa(binaryString));
  }

  uploadProfileImage() {
    var postData = {
      profileImage: this.doc_64textString
    };
    var id = localStorage.user_id;
    if (this.doc_64textString) {
      this.teacherService.uploadProfileImage(id, postData).subscribe(
        (data: any) => {
          console.log("this is the data", data);
          this.globalService.update_dp_service("changing the image");
          this.messageService.add({
            severity: "success",
            summary: "Profile Image updated"
          });
          this.getteacherdata(localStorage.user_id);
          this.doc_64textString = "";
        },
        (error: any) => {
          console.log("this is the error", error);
        }
      );
    }
  }
}
