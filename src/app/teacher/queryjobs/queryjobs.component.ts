import { Component, OnInit } from "@angular/core";
import { GlobalService } from "../../providers/global.service";
import { StudentService } from "../../providers/student.service";
import { DomSanitizer } from "@angular/platform-browser";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/components/common/messageservice";

import { environment } from "../../../environments/environment";
const APIEndpoint = environment.APIEndpoint;

@Component({
  selector: "app-queryjobs",
  templateUrl: "./queryjobs.component.html",
  styleUrls: ["./queryjobs.component.css"]
})
export class QueryjobsComponent implements OnInit {
  queryData: any;
  msgs: Message[] = [];
  dpUrl: any;
  constructor(
    public studentservice: StudentService,
    public sanetizer: DomSanitizer,
    public globalService: GlobalService
  ) {}

  ngOnInit() {
    this.getStudentsQueries();
    this.dpUrl = APIEndpoint + "public/student/dp/";
  }

  getStudentsQueries() {
    this.studentservice.getStudentsQueries().subscribe(
      (data: any) => {
        console.log("thisi s the data", data);
        this.queryData = data.data;
        for (var t = 0; t < this.queryData.length; t++) {
          this.queryData[t].question = this.sanetizer.bypassSecurityTrustUrl(
            this.queryData[t].question
          );
        }
      },
      (error: any) => {
        console.log("this is error", error);
      }
    );
  }
}
