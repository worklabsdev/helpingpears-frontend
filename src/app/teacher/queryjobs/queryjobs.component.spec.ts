import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryjobsComponent } from './queryjobs.component';

describe('QueryjobsComponent', () => {
  let component: QueryjobsComponent;
  let fixture: ComponentFixture<QueryjobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryjobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryjobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
