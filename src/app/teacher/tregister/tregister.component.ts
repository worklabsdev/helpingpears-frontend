import { Component, OnInit } from '@angular/core';
import  { TeacherService } from '../../providers/teacher.service';
import { Router,RouterModule } from '@angular/router';
import { StudentService } from '../../providers/student.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-tregister',
  templateUrl: './tregister.component.html',
  styleUrls: ['./tregister.component.css']
})
export class TregisterComponent implements OnInit {
  name :string;
  email:string;
  password : string;
  authemail:string;
  authpass:string;
  regLoading:Boolean =false;
  msgs: Message[] = [];
  regForm : FormGroup;
  constructor(public teacherService:TeacherService,public router:Router,private messageService: MessageService) { }

  ngOnInit() {
    this.regForm = new FormGroup({
      name: new FormControl('',Validators.required),
      email :new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)]),
    })
  }

  register(){
    if(this.regForm.valid) {
      this.regLoading=true;
      var data =  {
        name:this.regForm.value.name,
        email:this.regForm.value.email,
        password:this.regForm.value.password
      }
      console.log("this is data",data);
      this.teacherService.register(data).subscribe((data:any)=>{
        // if(data.token){
        //
        //   this.router.navigate(['/tprofile']);
        // }
        if(data.token){
          this.regLoading=false;
          localStorage.setItem('token',data.token);
          localStorage.setItem('user_name',data.userdata.name);
          localStorage.setItem('user_email',data.userdata.email);
          localStorage.setItem('user_id',data.userdata._id);
          localStorage.setItem('type','teacher');
          console.log("localStorage",localStorage.user_name);
          this.router.navigate(['/etprofile']);
          this.regForm.reset();
          this.messageService.add({severity:'success', summary:'Thank You for registering', detail:'please login'});
        }
      },(error:any)=>{
        this.regLoading=false;
        console.log("this is the error",error);
      })
    }
  }
}
