import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
afterChange:any;
  constructor(public router: Router) { }

  ngOnInit() {
    if(localStorage.getItem('token') && localStorage.getItem('user_name') && localStorage.getItem('user_email') && localStorage.getItem('user_id')){

      if(localStorage.getItem('type')=='teacher'){
        this.router.navigate(['/etprofile']);

      }else{
        this.router.navigate(['eprofile']);

      }
    }
  }
  slides = [
    {img: "./assets/images/student-bg.jpg"},
    {img: "./assets/images/student1-bg.jpg"}
  
  ];
  slideConfig = {
  "slidesToShow": 1, 
  "slidesToScroll": 1,
  "fade": true,
  "autoplay":true,
};
carsousel = [
  {img: "./assets/images/icon-1.png",heading:"Stet clita kasd ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."},
  {img: "./assets/images/icon-2.png",heading:"At vero eos et ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."},
  {img: "./assets/images/icon-3.png",heading:"At vero eos et ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."},
  {img: "./assets/images/icon-4.png",heading:"Stet clita kasd ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."},
  {img: "./assets/images/icon-5.png",heading:"At vero eos et ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."},
  {img: "./assets/images/icon-3.png",heading:"Stet clita kasd ",content:"At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est."}

];
carsouselConfig = {"slidesToShow": 5, "slidesToScroll": 1,
"autoplay":false,
responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 3,
      infinite: true,
      dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 2
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
],
};
testimonial = [
  {img: "http://placehold.it/79x79",heading:"Martin Jonson Design  Dev, Inc.",content:" Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat."},
  {img: "http://placehold.it/79x79",heading:"Martin Jonson Design  Dev, Inc.",content:" Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat."},
  {img: "http://placehold.it/79x79",heading:"Martin Jonson Design  Dev, Inc.",content:" Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat."}

];
testimonialConfig = {"slidesToShow": 1, "slidesToScroll": 1,
"autoplay":true,
"arrows":true
};
}
