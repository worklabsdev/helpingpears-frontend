import { Component, OnInit } from '@angular/core';
import { Router,RouterModule } from '@angular/router';
import { StudentService } from '../providers/student.service';
import { TeacherService } from '../providers/teacher.service';
import { FormGroup,FormControl,Validators } from '@angular/forms';
// import { StudentInterface } from './studentInterface';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers : [MessageService]
})
export class LoginComponent implements OnInit {
  name :string;
  email:string;
  password : string;
  authemail:string;
  authpass:string;
  regLoading:Boolean =false;
  msgs: Message[] = [];
  loginForm : FormGroup;
  regForm : FormGroup;
  userType:String;
  constructor(public studentService:StudentService,public teacherService:TeacherService,
    public router:Router,private messageService: MessageService) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)])
    })
    this.regForm = new FormGroup({
      name: new FormControl('',Validators.required),
      email :new FormControl('',[Validators.required,Validators.email]),
      age: new FormControl('',[Validators.required,Validators.pattern("^[0-9]*$")]),
      class: new FormControl('',[Validators.required]),
      password : new FormControl('',[Validators.required,Validators.minLength(7)]),
    })
  }

  authenticate(){
    if(this.loginForm.valid && this.userType=='student'){
      var authdata = {
        email:this.loginForm.value.email,
        password:this.loginForm.value.password
      }
      this.studentService.authenticate(authdata).subscribe((data:any)=>{
        console.log("this is data",data);
        if(data.token){
          localStorage.setItem('token',data.token);
          localStorage.setItem('user_name',data.userdata.firstname);
          localStorage.setItem('user_email',data.userdata.email);
          localStorage.setItem('user_id',data.userdata._id);
          console.log("this is the loc",localStorage.followedexpert);
          this.router.navigate(['eprofile']);
        }
        // else if(data.status==401) {
        //     this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        // }
      },(error:any)=>{
        if(error.status==401)
        this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        console.log("this is the error",error);
      })
      console.log("this is the auth data",authdata);
    }
    else if(this.loginForm.valid && this.userType=='teacher'){

      var authdata = {
        email:this.loginForm.value.email,
        password:this.loginForm.value.password
      }
      this.teacherService.authenticate(authdata).subscribe((data:any)=>{
        console.log("this is data",data);
        if(data.token){
          localStorage.setItem('token',data.token);
          localStorage.setItem('user_name',data.userdata.name);
          localStorage.setItem('user_email',data.userdata.email);
          localStorage.setItem('user_id',data.userdata._id);
          localStorage.setItem('type','teacher');
          console.log("localStorage",localStorage.user_name);
          this.router.navigate(['/etprofile']);
        }
        // else if(data.status==401) {
        //     this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        // }
      },(error:any)=>{
        if(error.status==401)
        this.messageService.add({severity:'error', summary:'Invalid Credentials'});
        console.log("this is the error",error);
      })
    }
  }

  register(){
    if(this.regForm.valid) {
      this.regLoading=true;
      var data =  {
        firstname:this.regForm.value.name,
        email:this.regForm.value.email,
        age:this.regForm.value.age,
        class:this.regForm.value.class,
        password:this.regForm.value.password
      }
      console.log("this is data",data);
      this.studentService.register(data).subscribe((data:any)=>{
        if(data.status==200){
          this.regLoading=false;
          this.regForm.reset();
          this.messageService.add({severity:'success', summary:'Thank You for registering', detail:'please login'});
        }
      },(error:any)=>{
        this.regLoading=false;
        console.log("this is the error",error);
      })
    }
  }
}
