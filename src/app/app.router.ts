import  { ModuleWithProviders } from '@angular/core';
import  { RouterModule,Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './student/profile/profile.component';
import { AuthGuard } from './auth.guard';
import { EditprofileComponent } from './student/editprofile/editprofile.component';
import { StucontactComponent } from './student/stucontact/stucontact.component';
import { AskqueryComponent } from './student/askquery/askquery.component';
import { SinglequeryComponent } from './student/singlequery/singlequery.component';
import { PqueriesComponent } from './public/pqueries/pqueries.component';
// Teacher components
import { TloginComponent } from './teacher/tlogin/tlogin.component';
import { TprofileComponent } from './teacher/tprofile/tprofile.component';
import { TeditprofileComponent } from './teacher/teditprofile/teditprofile.component';
import { TregisterComponent } from './teacher/tregister/tregister.component';
import { RegisterComponent } from './student/register/register.component';
import { SAdminQUeryComponent } from './student/AdminQuery/s-admin-query/s-admin-query.component';
import { TContactAdminComponent } from './teacher/t-contact-admin/t-contact-admin.component';
import { TsAdminQueryComponent } from './teacher/ts-admin-query/ts-admin-query.component';
import { QueryjobsComponent } from './teacher/queryjobs/queryjobs.component';
import { SingleQueryJobComponent } from './teacher/single-query-job/single-query-job.component';
import { TMyJobsComponent } from './teacher/t-my-jobs/t-my-jobs.component';
import { PriceComponent } from './pricing/pricing.components';
import { OurVisionComponent } from './our-vision/our-vision.component';
import { PreRegisterComponent } from './register/register.component';
// import  {RegisterComponent } from './register/register.component';





const appRoutes:Routes =[
  { path : '', component:  HomeComponent},
  { path:'login',component:LoginComponent },
  { path: 'scontact', component:StucontactComponent },
  { path: 'scontact/:id',component:SAdminQUeryComponent },
  { path : 'signup',component:RegisterComponent},
  { path : 'queries',component: PqueriesComponent},
  { path : 'sprofile',component:ProfileComponent ,canActivate:[AuthGuard]},
  { path : 'squery',component:AskqueryComponent ,canActivate:[AuthGuard]},
  { path : 'squery/:slug', component:SinglequeryComponent,canActivate:[AuthGuard]},
  { path : 'eprofile',component: EditprofileComponent, canActivate:[AuthGuard]},
  //  teachers routes
  { path : 'tlogin', component: TloginComponent},
  { path : 'tprofile', component: TprofileComponent},
  { path : 'etprofile',component: TeditprofileComponent },
  { path : 'tsignup', component: TregisterComponent},
  { path : 'tcontact', component:TContactAdminComponent },
  { path : 'tcontact/:id', component:TsAdminQueryComponent },
  { path : 'jobs', component: QueryjobsComponent},
  { path : 'job/:slug', component: SingleQueryJobComponent},
  { path : 'myjobs', component: TMyJobsComponent},
  { path : 'pricing', component: PriceComponent},
  { path : 'our-vision', component: OurVisionComponent},
  { path : 'register', component: PreRegisterComponent},
 
  
]

export const AppRouting = RouterModule.forRoot(appRoutes);
