
if (!AgoraRTC.checkSystemRequirements()) {
  alert("Your browser does not support WebRTC!");
}

/* select Log type */
// AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.NONE);
// AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.ERROR);
// AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.WARNING);
// AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.INFO);  
// AgoraRTC.Logger.setLogLevel(AgoraRTC.Logger.DEBUG);

/* simulated data to proof setLogLevel() */
AgoraRTC.Logger.error('this is error');
AgoraRTC.Logger.warning('this is warning');
AgoraRTC.Logger.info('this is info');
AgoraRTC.Logger.debug('this is debug');

var formComponent1 = null;
var formComponent2 = null;

var startTime, endTime, payment, videostart;

var client, localStream, camera, microphone;

var audioSelect, videoSelect;

function detect() {

  audioSelect = document.querySelector('select#audioSource');
  videoSelect = document.querySelector('select#videoSource');
  console.log('audioSelect', audioSelect)

  AgoraRTC.getDevices(function (devices) {
    for (var i = 0; i !== devices.length; ++i) {
      var device = devices[i];
      var option = document.createElement('option');
      option.value = device.deviceId;
      if (device.kind === 'audioinput') {
        option.text = device.label || 'microphone ' + (audioSelect.length + 1);
        audioSelect.appendChild(option);
        console.log('audioSelectaaaaaaaaaaaaaaa', option)
      } else if (device.kind === 'videoinput') {
        option.text = device.label || 'camera ' + (videoSelect.length + 1);
        videoSelect.appendChild(option);
      } else {
        console.log('Some other kind of source/device: ', device);
      }
    }
  });

}
function join() {
  payment = false;
  console.log('audioSelect', audioSelect)

  document.getElementById("join").disabled = true;
  document.getElementById("video").disabled = true;
  var channel_key = null;

  // console.log("Init AgoraRTC client with App ID: " + appId.value);
  client = AgoraRTC.createClient({ mode: 'live' });
  client.init("482f3b0826bb40758dbbe87e5058d7ef", function () {
    console.log("AgoraRTC client initialized");
    client.join(channel_key, channel.value, null, function (uid) {
      console.log("User " + uid + " join channel successfully");

      if (document.getElementById("video").checked) {
        camera = videoSource.value;
        microphone = audioSource.value;
        localStream = AgoraRTC.createStream({ streamID: uid, audio: true, cameraId: camera, microphoneId: microphone, video: document.getElementById("video").checked, screen: false });
        //localStream = AgoraRTC.createStream({streamID: uid, audio: false, cameraId: camera, microphoneId: microphone, video: false, screen: true, extensionId: 'minllpmhdgpndnkomcoccfekfegnlikg'});
        if (document.getElementById("video").checked) {
          localStream.setVideoProfile('720p_3');
        }

        // The user has granted access to the camera and mic.
        localStream.on("accessAllowed", function () {
          console.log("accessAllowed");
        });

        // The user has denied access to the camera and mic.
        localStream.on("accessDenied", function () {
          console.log("accessDenied");
        });

        localStream.init(function () {
          console.log("getUserMedia successfully");
          localStream.play('agora_local');
          client.publish(localStream, function (err) {
            console.log("Publish local stream error: " + err);
          });

          client.on('stream-published', function (evt) {
            console.log("Publish local stream successfully");
          });
        }, function (err) {
          console.log("getUserMedia failed", err);
        });
      }
    }, function (err) {
      console.log("Join channel failed", err);
    });
  }, function (err) {
    console.log("AgoraRTC client init failed", err);
  });

  channelKey = "";
  client.on('error', function (err) {
    console.log("Got error msg:", err.reason);
    if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
      client.renewChannelKey(channelKey, function () {
        console.log("Renew channel key successfully");
      }, function (err) {
        console.log("Renew channel key failed: ", err);
      });
    }
  });


  client.on('stream-added', function (evt) {
    var stream = evt.stream;
    console.log("New stream added: " + stream.getId());
    console.log("Subscribe ", stream);
    client.subscribe(stream, function (err) {
      console.log("Subscribe stream failed", err);
    });
  });

  client.on('stream-subscribed', function (evt) {
    document.getElementById('agora_local').style.display = 'block';
    var stream = evt.stream;
    var d = new Date();
    videostart = true;
    startTime = d.getTime();
    sessionStorage.setItem('toklight', startTime)
    console.log('streeeeam Timeeeeeeeeeeeee startttttttt', startTime)

    console.log("Subscribe remote stream successfully: " + stream.getId());
    if ($('div#video #agora_remote' + stream.getId()).length === 0) {
      // $('div#video').append('<div id="agora_remote'+stream.getId()+'" style="float:left; width:810px;height:607px;display:inline-block; position:static !important;"></div>');
      $('div#video').append('<div id="agora_remote' + stream.getId() + '" style="float:left; width:810px;height:500px;display:inline-block;position:relative;margin-bottom:20px;"></div>');
    }
    stream.play('agora_remote' + stream.getId());

  });

  client.on('stream-removed', function (evt) {
    var stream = evt.stream;
    stream.stop();
    $('#agora_remote' + stream.getId()).remove();
    var d = new Date();
    endTime = d.getTime();
    console.log('2222222222222222', endTime)
    leave();
    console.log("Remote stream is removed " + stream.getId());
  });

  client.on('peer-leave', function (evt) {
    var stream = evt.stream;
    if (stream) {
      stream.stop();
      $('#agora_remote' + stream.getId()).remove();
      console.log(evt.uid + " leaved from this channel");

      var d = new Date();
      endTime = d.getTime();
      console.log('3333333333333333333', endTime)

      timeConsumed();
    }
  });
}

function leave() {
  document.getElementById('agora_local').style.display = 'none';
  document.getElementById("join").enabled = true;
  // document.getElementById("video").disabled = true;
  document.getElementById("leave").disabled = true;
  client.leave(function () {
    console.log("Leavel channel successfully");
    var d = new Date();
    endTime = d.getTime();
    console.log('3333333333333333333', endTime)

    timeConsumed();

  }, function (err) {
    var d = new Date();
    endTime = d.getTime();
    console.log('2222222222222222', endTime)
    timeConsumed();
    console.log("Leave channel failed");
  });
}

function timeConsumed() {
  videostart = false;
  console.log('startTime1111111', startTime)
  console.log('endTime2222222222', endTime)
  // if(!startTime){
  //   startTime=sessionStorage.getItem('toklight');
  // }
  // sessionStorage.getItem('toklight')
  console.log('startTime22222222222', startTime)

  var difference = endTime - startTime;
  var totalTimeCall = Math.floor(difference / 1000);
  // localStorage.setItem('totalTimeCall',totalTimeCall)
  // console.log('lllllllllllllllllllllll',daysDifference);
  localStorage.setItem("2", "2")
  console.log('totalTimeCall', totalTimeCall)
  console.log('formcomponent1', formComponent1)
  console.log('formcomponent2', formComponent2)
  // formComponent1.payment(totalTimeCall);
  console.log("paymenttttttttt", payment)
  if (formComponent1 != null) {
    formComponent1.payment(totalTimeCall);

  } else {
    formComponent2.payment(totalTimeCall);
  }

  // payment(totalTimeCall)
}
// window.addEventListener("beforeunload", function (e) {
//   var confirmationMessage = "\o/";
//   (e || window.event).returnValue = confirmationMessage; //Gecko + IE
//   return confirmationMessage;                            //Webkit, Safari, Chrome
// });


// window.onbeforeunload = function(e) {
//   e.preventDefault();
//   alert(1);
//   console.log(2)
//   if(videostart==true){
//     localStorage.setItem("21","21")
//     client.on('peer-leave', function (evt) {
//       var stream = evt.stream;
//       if (stream) {
//         stream.stop();
//         $('#agora_remote' + stream.getId()).remove();
//         console.log(evt.uid + " leaved from this channel");

//         var d = new Date();
//         endTime = d.getTime();
//         console.log('3333333333333333333',endTime)

//         timeConsumed();
//       }
//     });
//     // leave();
//     // var d = new Date();
//     // endTime = d.getTime();
//     // console.log('3333333333333333333',endTime)

//     // timeConsumed();
//     }
// }

// function myFunction(){
//   localStorage.setItem("1","1");

//   alert("2")
//   alert("3");

// }
function publish() {
  document.getElementById("publish").disabled = true;
  document.getElementById("unpublish").disabled = false;
  client.publish(localStream, function (err) {
    console.log("Publish local stream error: " + err);
  });
}

function unpublish() {
  document.getElementById("publish").disabled = false;
  document.getElementById("unpublish").disabled = true;
  client.unpublish(localStream, function (err) {
    console.log("Unpublish local stream failed" + err);
  });
}


  // function getDevices() {

  //   AgoraRTC.getDevices(function (devices) {
  //     for (var i = 0; i !== devices.length; ++i) {
  //       var device = devices[i];
  //       var option = document.createElement('option');
  //       option.value = device.deviceId;
  //       if (device.kind === 'audioinput') {
  //         option.text = device.label || 'microphone ' + (audioSelect.length + 1);
  //         audioSelect.appendChild(option);
  //         console.log('audioSelectaaaaaaaaaaaaaaa',option)
  //       } else if (device.kind === 'videoinput') {
  //         option.text = device.label || 'camera ' + (videoSelect.length + 1);
  //         videoSelect.appendChild(option);
  //       } else {
  //         console.log('Some other kind of source/device: ', device);
  //       }
  //     }
  //   });
  // }

  //audioSelect.onchange = getDevices;
  //videoSelect.onchange = getDevices;
  //  getDevices();